<?php

namespace Drupal\field_3d\Plugin\Field\FieldWidget;


use Drupal\file\Plugin\Field\FieldWidget\FileWidget;


/**
 * Plugin implementation of the 'file_3d_generic' widget.
 *
 * @FieldWidget(
 *   id = "file_3d_generic",
 *   label = @Translation("File 3D"),
 *   field_types = {
 *     "file_3d"
 *   }
 * )
 */
class File3DWidget extends FileWidget {

}
