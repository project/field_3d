<?php

namespace Drupal\field_3d\Plugin\Field\FieldType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Plugin\Field\FieldType\FileItem;

/**
 * Plugin implementation of the '3D' field type.
 *
 * @FieldType(
 *   id = "file_3d",
 *   label = @Translation("3D File"),
 *   description = @Translation("This field is made for 3D files."),
 *   category = @Translation("Reference"),
 *   default_widget = "file_3d_generic",
 *   default_formatter = "file_3d_babylon",
 *   list_class = "\Drupal\file\Plugin\Field\FieldType\FileFieldItemList",
 * )
 */
class File3DItem extends FileItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
        'file_extensions' => 'gltf,glb,babylon,obj,stl',
      ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::fieldSettingsForm($form, $form_state);
    // File extensions are fixed for 3D Files
    $element['file_extensions']['#access'] = FALSE;
    return $element;
  }

}
